**IMPORTANT NOTICE:** this app is work in progress. You are free to use it but be aware that errors may occur and and
major parts of the app might change in the future.

# License Checker

A tool for analysing dependencies of a golang project. It checks the licenses of 3rd party packages and summerise it
into a single file.

## Why another license checker?

There are several tools on github that already meet parts of my requirements. The problem with most of them for me is
that they are either very big because they want to deal with many different dependency managers (go get, npm, composer,
pip, ...), hard to use or simply not able to create a summary file with all third party licenses, which is sometimes
required if you ship your software in binary form e.g. inside docker images.

## Installation

### Using Go

```
go install gitlab.com/bosi/license-checker@latest
```

(This method requires a locally installed golang with at lease v1.17)

### Using pre-compiled binary

Just download the version you want from the [releases](https://gitlab.com/bosi/license-checker/-/releases) section.

## Usage

- go inside your go app
- install your dependencies: `go get -v ./...`
- run the license-checker (via `${GOPATH}/bin/license-checker` or `./license-checker`)
    - the tool will check against several allowed licenses (can be adjusted by using cli param: `-allowed MIT,ISC`)
    - a license summary file will be created (can be adjusted by using cli param: `-outpout LICENSES-3RD-PARTY`)
    - you can specify packages which will not be checked: e.g. to exclude `github.com/stretchr/testify` you may use
      `-exclude=testify`

## Used 3rd Party Libraries

This app uses several 3rd party libraries. Their licenses can be
viewed [here](https://bosi.gitlab.io/license-checker/licenses-3rd-party.txt) (gitlab pages).

## Disclaimer

This app should be considered as *work in progress*. This means that the source code might be really messy in some
functions and test coverage is not the best (or not present at all).
