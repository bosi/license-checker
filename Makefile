include project-templates/base.mk
include .env
export

project-templates/base.mk:
	@cp -ar ~/.dotfiles/projects/golang ./project-templates

.env:
	@cp .env.example .env
