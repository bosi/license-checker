package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/google/licenseclassifier"
	"github.com/google/licenseclassifier/tools/identify_license/backend"
)

type licenseInfo struct {
	packageName    string
	packageVersion string
	packagePath    string
	licenses       []string
	err            error
}

const (
	defaultLicenseSummaryPath = "licenses-3rd-party"

	colorReset  = "\033[0m"
	colorRed    = "\033[31m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"

	cliOutputMinWith = 27
)

var (
	licenseFilePatterns = []string{
		"%s/LICENSE",
		"%s/LICENCE",
		"%s/License",
		"%s/Licence",
		"%s/LICENSE.md",
		"%s/LICENSE.txt",
		"%s/License.md",
		"%s/Licence.txt",
	}

	defaultAllowedLicenses = []string{
		"MIT",
		"Apache-2.0",
		"BSD-3-Clause",
		"BSD-2-Clause",
		"ISC",
		"MPL-2.0",
	}

	allowedLicenses    []string
	licenseSummaryPath string
	silent             bool
	excludedPackages   []string
)

func init() {
	path := flag.String("output", defaultLicenseSummaryPath, "path to the license summary file")
	licenses := flag.String("allowed", "", "comma separated list of allowed licenses")
	silentFlag := flag.Bool("silent", false, "if set, only errors will be printed")
	excluded := flag.String("exclude", "", "comma seperated list of packages which will not be checked")
	flag.Parse()

	if *licenses == "" {
		allowedLicenses = defaultAllowedLicenses
	} else {
		allowedLicenses = strings.Split(*licenses, ",")
	}

	licenseSummaryPath = *path
	silent = *silentFlag
	excludedPackages = strings.Split(*excluded, ",")
}

//nolint:gocognit
func main() {
	c := exec.Command("go", "list", "-m", "-f", "{{.Path}}|{{.Dir}}|{{.Version}}|{{.Main}}", "all")
	out, _ := c.Output()
	ls, length := licenseInfos(out)

	exitCode := 0
	licenseSummary, _ := os.Create(licenseSummaryPath)
	fmt.Fprintf(licenseSummary, "###################################\n")
	fmt.Fprintf(licenseSummary, "# Licenses of 3rd Party Libraries #\n")
	fmt.Fprintf(licenseSummary, "###################################\n\n")

	for _, l := range ls {
		pattern := fmt.Sprintf("%%-%ds | ", length)
		if !silent {
			fmt.Printf(pattern, l.packageName)
		}

		if isPackageExcluded(l.packageName) {
			fmt.Printf("%s%s%s\n", colorYellow, "excluded", colorReset)
			continue
		}

		if len(l.licenses) == 0 {
			if !silent {
				fmt.Printf("%sunknown license%s\n", colorRed, colorReset)
			}
			exitCode = 1
		} else {
			for _, license := range l.licenses {
				var color string
				if isLicenseAllowed(license) {
					color = colorGreen
				} else {
					color = colorRed
					exitCode = 1
				}

				if !silent {
					fmt.Printf("%s%s%s", color, license, colorReset)
				}
			}
			if !silent {
				fmt.Println()
			}

			srcLicense, _ := os.Open(l.licenseFilePath())
			fmt.Fprintf(licenseSummary, "Package: %s\n", l.packageName)
			fmt.Fprintf(licenseSummary, "License: \n\n")
			if _, err := io.Copy(licenseSummary, srcLicense); err != nil {
				log.Fatalf("could not copy license text into summary file: %s\n", err)
			}
			fmt.Fprintf(licenseSummary, "\n--------------------\n")
		}

	}

	if !silent {
		fmt.Printf("%s\n", strings.Repeat("―", length+cliOutputMinWith))
	}

	os.Exit(exitCode)
}

func licenseInfos(out []byte) ([]licenseInfo, int) {
	lines := strings.Split(string(out), "\n")

	ls := []licenseInfo{}

	if !silent {
		fmt.Print("Processing packages")
	}

	length := 0
	for _, line := range lines {
		if !silent {
			fmt.Printf(".")
		}
		if line == "" {
			continue
		}
		parts := strings.Split(line, "|")
		if parts[3] == "true" {
			// skip main module
			continue
		}

		newLi := licenseInfo{
			packageName:    parts[0],
			packageVersion: parts[2],
			packagePath:    parts[1],
		}

		if newLi.packagePath == "" {
			continue
		}

		if length < len(newLi.packageName) {
			length = len(newLi.packageName)
		}

		newLi.lookupLicense()
		ls = append(ls, newLi)
	}
	if !silent {
		fmt.Print("done\n\nResults:\n")

		fmt.Printf("%s\n", strings.Repeat("―", length+cliOutputMinWith))
	}
	return ls, length
}

func isPackageExcluded(p string) bool {
	if len(excludedPackages) == 0 {
		return false
	}

	for _, excludedPackage := range excludedPackages {
		if excludedPackage != "" && strings.Contains(p, excludedPackage) {
			return true
		}
	}

	return false
}

func isLicenseAllowed(license string) bool {
	for _, al := range allowedLicenses {
		if strings.EqualFold(al, license) {
			return true
		}
	}

	return false
}

func (li *licenseInfo) licenseFilePath() string {
	for _, pattern := range licenseFilePatterns {
		path := fmt.Sprintf(pattern, li.packagePath)
		if _, err := os.Stat(path); err == nil {
			return path
		}
	}

	return ""
}

func (li *licenseInfo) lookupLicense() {
	licensePath := li.licenseFilePath()
	if licensePath == "" {
		li.err = errors.New("no license file found")
		return
	}

	be, err := backend.New(licenseclassifier.DefaultConfidenceThreshold, false)
	if err != nil {
		log.Fatalf("cannot create license classifier: %v", err)
	}

	log.SetOutput(io.Discard)
	if errs := be.ClassifyLicenses([]string{licensePath}, false); errs != nil {
		for _, err := range errs {
			log.Printf("classify license failed: %v", err)
		}
		log.Fatal("cannot classify licenses")
	}
	log.SetOutput(os.Stdout)

	results := be.GetResults()

	if len(results) == 0 {
		log.Fatalf("Couldn't classify license(s) insode %s", licensePath)
	}

	for _, r := range results {
		li.licenses = append(li.licenses, r.Name)
	}
}
