module gitlab.com/bosi/license-checker

go 1.17

require github.com/google/licenseclassifier v0.0.0-20210722185704-3043a050f148

require github.com/sergi/go-diff v1.2.0 // indirect
